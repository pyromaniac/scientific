# Copyright 2010 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Reference-LAPACK project=lapack tag=v${PV} ]
require cmake alternatives

SUMMARY="Basic Linear Algebra Subprograms"
HOMEPAGE+=" https://www.netlib.org/${PN}"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# Tests are dependant on lapack which we dont build
RESTRICT="test"

DEPENDENCIES="
    build+run:
        sys-libs/libgfortran:=
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_DEPRECATED:BOOL=FALSE
    -DBUILD_COMPLEX:BOOL=TRUE
    -DBUILD_COMPLEX16:BOOL=TRUE
    -DBUILD_DOUBLE:BOOL=TRUE
    -DBUILD_INDEX64:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_SINGLE:BOOL=TRUE
    -DBUILD_TESTING:BOOL=FALSE
    -DCBLAS:BOOL=FALSE
    -DCMAKE_Fortran_COMPILER=${FORTRAN}
)

src_prepare() {
    cmake_src_prepare

    # Remove LAPACK
    edo sed \
        -e '/add_subdirectory(SRC)/d' \
        -e '/set(ALL_TARGETS ${ALL_TARGETS} ${LAPACKLIB})/d' \
        -e '/set(ALL_TARGETS ${ALL_TARGETS} ${TMGLIB})/d' \
        -i "${CMAKE_SOURCE}"/CMakeLists.txt
}

src_install() {
    cmake_src_install

    # Remove LAPACK
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib/cmake
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/pkgconfig/lapack.pc

    # Name of BLAS library to be installed.
    BLAS_LIBRARY="libblas-netlib.so"
    edo mv "${IMAGE}"/usr/$(exhost --target)/lib/{libblas.so,${BLAS_LIBRARY}}
    alternatives_for blas blas-netlib \
        0 /usr/$(exhost --target)/lib/libblas.so ${BLAS_LIBRARY}
}

