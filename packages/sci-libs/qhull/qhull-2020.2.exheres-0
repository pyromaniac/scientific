# Copyright 2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github cmake

SUMMARY="General dimension convex hull programs"
DESCRIPTION="
Qhull is a general dimension convex hull program that reads a set of points from stdin, and outputs
the smallest convex set that contains the points to stdout. It also generates Delaunay
triangulations, Voronoi diagrams, furthest-site Voronoi diagrams, and halfspace intersections about
a point.
"

LICENCES="Qhull"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_STATIC_LIBS:BOOL=FALSE
    -DDOC_INSTALL_DIR:PATH=/usr/share/doc/${PNVR}
    -DLINK_APPS_SHARED:BOOL=TRUE
    -DMAN_INSTALL_DIR:PATH=/usr/share/man/man1
)

src_install() {
    cmake_src_install

    # remove includes for static libraries we don't build/install
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/include/libqhull{,cpp}
    # remove pkgconfig files for static libraries we don't build/install
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/pkgconfig/qhull{static,static_r,cpp}.pc
}

